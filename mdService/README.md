# Message Driven Service
The fundamental concept of message driven service (mds) is to provide a strict interface providing as much freedom for the service provider as possible.

Here, the idea of the  "Representational State Transfer" (REST) is again implemented, but this time agnostic to the world wide web (www).

Here, 4 main URLs are sufficient in order to declare the complete service interface.

Input
  I
Output
  S
  E

Description
  D


